FROM openjdk:8-jdk-alpine
EXPOSE 8080
ARG JAR_FILE=target/*.jar
ADD ${JAR_FILE} directory-explorer/app.jar
COPY scrpt.sh /scripts/scrpt.sh
ENTRYPOINT ["java","-jar","directory-explorer/app.jar"]