# directory-explorer

Entersekt assessment


## How to build

To build this project, make sure you have Java 8, maven and docker installed. Then navigate to the root directory of the project. 

Run the following Maven command to package the project into a .jar executable

```
mvn package
```

When the build is finished there should be a target folder generated with the .jar executable

From here we need to dockerize the executable. Run the following command

```
docker build -t directory-explorer .
```
This will build the docker image using the openjdk:8 image specified in the Dockerfile


## How to run

Once this is done you can run the container using the following container

```
docker run -dp 8080:8080 directory-explorer
```

The service should be running in port 8080.

To check if the project is running you call health check to see if the service is up. Got to http://localhost:8080/actuator/health. If status = UP then the service is running.
You can start exploring.

Call directory/explore to start exploring the container's file system.

```
GET - http://localhost:8080/directory/explorer

BODY - 
{
    path: '/'
}
```