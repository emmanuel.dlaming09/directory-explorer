package com.entersekt.directoryexplorer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DirectoryItem {
    private String name;
    private FileType type;
    private String permissions;
    private int links;
    private String owner;
    private String group;
    private long size;
    private String time;
}
