package com.entersekt.directoryexplorer.model;

public enum FileType {

    REGULAR_FILE('-'),
    BLOCK_SPECIAL_FILE('b'),
    CHARACTER_SPECIAL_FILE('c'),
    DIRECTORY('d'),
    SYMBOLIC_LINK('l'),
    NETWORK_FILE('n'),
    FIFO('p'),
    SOCKET('s');

    private Character value;

    private FileType(Character v){
        this.value = v;
    }

    public Character getValue(){
        return value;
    }

    public static FileType fromCharacter(Character c){
        for( FileType t: FileType.values()){
            if(t.value == c) return t;
        }
        return null;
    }
}
