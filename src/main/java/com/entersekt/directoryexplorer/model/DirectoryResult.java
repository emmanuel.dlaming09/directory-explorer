package com.entersekt.directoryexplorer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DirectoryResult {
    private PathInput input;
    private List<DirectoryItem> contents;
}
