package com.entersekt.directoryexplorer.services;

import com.entersekt.directoryexplorer.exceptions.InternalServerException;
import com.entersekt.directoryexplorer.exceptions.NotFoundException;
import com.entersekt.directoryexplorer.model.DirectoryItem;
import com.entersekt.directoryexplorer.model.FileType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class DirectoryService {
    Logger logger = LoggerFactory.getLogger(DirectoryService.class);
    public List<DirectoryItem> fetchListing(String path) throws NotFoundException, InternalServerException {
        logger.info("Fetching attributes for path " + path);
        ProcessBuilder processBuilder = new ProcessBuilder();

        processBuilder.command("/bin/sh", "-c", String.format("scripts/scrpt.sh %s", path));
        List<DirectoryItem> results = new ArrayList<>();
        try {

            Process process = processBuilder.start();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line = reader.readLine();

            if(line.equals("404"))
            {
                logger.error("The path: " + path + " is not a valid path");
                throw new NotFoundException();
            }
            logger.info("Path valid! Getting file attributes");
            while ((line = reader.readLine()) != null) {
                results.add(createItem(line));
            }

        } catch (IOException e) {
            logger.error("There was an error!!");
            throw new InternalServerException();
        }
        logger.info("Returning results.");
        return results;
    }

    public DirectoryItem createItem(String line){
        String[] attr = line.split("\\s+");

        return DirectoryItem.builder()
                .type(FileType.fromCharacter(attr[0].charAt(0)))
                .permissions(attr[0].substring(0))
                .links(Integer.parseInt(attr[1]))
                .owner(attr[2])
                .group(attr[3])
                .size(Long.parseLong(attr[4]))
                .time(attr[5]+ " " + attr[6] + " " + attr[7])
                .name(attr[8])
                .build();
    }

}
