package com.entersekt.directoryexplorer.exceptions;

public class NotFoundException extends Exception{
    public NotFoundException(){
        super("The resource requested was not found.");
    }
}
