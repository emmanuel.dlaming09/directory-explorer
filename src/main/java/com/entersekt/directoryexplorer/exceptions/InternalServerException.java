package com.entersekt.directoryexplorer.exceptions;

public class InternalServerException extends Exception{
    public InternalServerException(){
        super("The request was not completed due to an internal error on the server side.");
    }

}
