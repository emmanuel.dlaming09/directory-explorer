package com.entersekt.directoryexplorer.controllers;


import com.entersekt.directoryexplorer.exceptions.InternalServerException;
import com.entersekt.directoryexplorer.exceptions.NotFoundException;
import com.entersekt.directoryexplorer.model.DirectoryItem;
import com.entersekt.directoryexplorer.model.PathInput;
import com.entersekt.directoryexplorer.model.DirectoryResult;
import com.entersekt.directoryexplorer.services.DirectoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("directory")
public class DirectoryController {
    Logger logger = LoggerFactory.getLogger(DirectoryController.class);
    @Autowired
    DirectoryService service;

    @GetMapping("/explore")
    public ResponseEntity<?> list(@RequestBody PathInput path){
        logger.info("Starting DirectoryController /explore");
        try {
            return new ResponseEntity<DirectoryResult>(DirectoryResult.builder()
                    .input(path)
                    .contents(service.fetchListing(path.getPath()))
                    .build(), HttpStatus.OK);
        } catch (NotFoundException | InternalServerException e) {
            logger.error("ERROR: " + e);
            return new ResponseEntity<com.entersekt.directoryexplorer.model.Error>(new com.entersekt.directoryexplorer.model.Error(e.getMessage()), HttpStatus.NOT_FOUND);
        }
    }
}
