package com.entersekt.directoryexplorer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirectoryExplorerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DirectoryExplorerApplication.class, args);
	}

}
